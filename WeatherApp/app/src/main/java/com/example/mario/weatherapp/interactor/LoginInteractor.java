package com.example.mario.weatherapp.interactor;

import com.example.mario.weatherapp.presenter.LoginPresenter;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

/**
 * Created by Mario on 11/05/2016.
 */
public class LoginInteractor implements LoginInteractorInterface {

    //Firebase URL
    private Firebase userRef = new Firebase("https://personal-weather-app.firebaseio.com/");

    public LoginInteractor() {
    }

    //Login interactor interface
    //This method try to login on firebase, calling onAuthenticated if success and
    //onAuthenticationError if not.
    @Override
    public void attemptToLogin(String email, String password, final Callback callback) {
        userRef.authWithPassword(email, password, new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                callback.onSuccess();
            }
            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                callback.onError(firebaseError.getMessage());
            }
        });
    }
}