package com.example.mario.weatherapp.presenter;

import android.app.Activity;

import com.example.mario.weatherapp.interactor.LoginInteractorInterface;
import com.example.mario.weatherapp.interactor.LoginInteractor;


/**
 * Created by Mario on 10/05/2016.
 */
public class LoginPresenter implements LoginPresenterInterface {

    private View view;
    private final LoginInteractor interactor;

    public LoginPresenter() {
        interactor = new LoginInteractor();
    }

    public void setView(View view) {
        if (view == null) {
            throw new IllegalArgumentException("You can't set a null view");
        }
        this.view = view;
    }

    // Login presenter interface
    // This method comunicate the ui with the interactor with a callback.
    @Override
    public void receiveUserLogin(String email, String password) {
        interactor.attemptToLogin(email, password, new LoginInteractorInterface.Callback() {
            @Override
            public void onSuccess() {
                view.onLoadSuccess();
            }

            @Override
            public void onError(String error) {
                view.onLoadError(error);
            }
        });
    }

    public interface View {
        void onLoadSuccess();
        void onLoadError(String error);
    }
}
