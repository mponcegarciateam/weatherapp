package com.example.mario.weatherapp.interactor;

import com.example.mario.weatherapp.model.WeatherData;

/**
 * Created by Mario on 11/05/2016.
 */
public interface WeatherInteractorInterface {

    interface Callback {
        void onSuccess(WeatherData weather);
        void onError(String error);
    }
}
