package com.example.mario.weatherapp.interactor;

import com.example.mario.weatherapp.interactor.executor.MainThread;
import com.example.mario.weatherapp.model.WeatherData;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Executor;

/**
 * Created by Mario on 11/05/2016.
 */
public class WeatherInteractor implements WeatherInteractorInterface, Runnable {

    private static final String WEATHER_URL_LONGITUDE = "lon=";
    private static final String WEATHER_URL_LATITUDE = "lat=";
    private static final String WEATHER_URL_LONANDLAT = "&";
    private static final String OPEN_WEATHER_MAP_API =
            "http://api.openweathermap.org/data/2.5/weather?";
    private static final String WEATHER_API = "&APPID=504cca572d40ceee6209479e0bce4631";
    private static final String WEATHER_ERROR = "No se ha podido cargar la informacion de su ubicacion.";
    private Executor executor;
    private MainThread mainThread;
    private double latitude;
    private double longitude;
    private Callback callback;
    private WeatherData weather;

    public WeatherInteractor(double latitude, double longitude, Executor executor, MainThread mainThread) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.executor = executor;
        this.mainThread = mainThread;
    }

    // WeatherInteractor methods
    public String doubleToString(double value) {
        return String.valueOf(value);
    }

    //This method concatenate the weather URL and the latlon information.
    public String getLocation (String latitude, String longitude) {
        return WEATHER_URL_LATITUDE + latitude + WEATHER_URL_LONANDLAT + WEATHER_URL_LONGITUDE + longitude;
    }

    public void execute(Callback callback) {
        this.callback = callback;
        //call method override "run" from interface runnable
        this.executor.execute(this);
    }

    //runnable method
    @Override
    public void run() {
        String lonString = doubleToString(longitude);
        String latString = doubleToString(latitude);
        String ubication = getLocation(latString, lonString);
        URL url;
        try {
            // This will concatenate the URL, latlon info and the API code that give you access to
            // your weather information.
            url = new URL(OPEN_WEATHER_MAP_API + ubication + WEATHER_API);
            Reader reader = new InputStreamReader(url.openStream());
            weather = WeatherData.getWeatherInfo(reader);
            notityDataLoaded(weather);
        } catch (MalformedURLException e1) {
            notifyError();
        } catch (IOException e1) {
            notifyError();
        }
    }

    //This method is called if the method runnable is able to get a WeatherData object.
    private void notityDataLoaded(final WeatherData weather) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(weather);
            }
        });
    }

    //This method is callef if the method runnable is not able to get a WeatherData Object.
    private void notifyError() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError(WEATHER_ERROR);
            }
        });
    }
}
