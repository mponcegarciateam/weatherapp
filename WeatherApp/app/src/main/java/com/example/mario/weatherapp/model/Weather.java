package com.example.mario.weatherapp.model;

/**
 * Created by Mario on 25/05/2016.
 */

public class Weather {

    private final static String ICON_ADDR = "http://openweathermap.org/img/w/";
    private final static String ICON_FORMAT = ".png";
    String description;
    String icon;

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return ICON_ADDR + icon + ICON_FORMAT;
    }

}
