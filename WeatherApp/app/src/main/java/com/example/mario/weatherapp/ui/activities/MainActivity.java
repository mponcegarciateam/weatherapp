package com.example.mario.weatherapp.ui.activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.example.mario.weatherapp.R;
import com.example.mario.weatherapp.presenter.LoginPresenter;

public class MainActivity extends AppCompatActivity implements LoginPresenter.View {

    private EditText etUser;
    private EditText etPassword;
    private Button btLogin;
    private ProgressBar progressBar;
    private LoginPresenter presenter;

    //AppCompatActivity methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setItems();
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinProgressBar();
                String email = etUser.getText().toString();
                String password = etPassword.getText().toString();
                logTheUserIn(email, password);
            }
        });
        stopProgressBar();
    }

    //MainActivity methods
    public void setItems (){
        etUser = (EditText) findViewById(R.id.etUser);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btLogin = (Button) findViewById(R.id.btLogin);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    // load and set the presenter
    public void LoadLoginPresenter (){
        presenter = new LoginPresenter();
        presenter.setView(this);
    }
    public void logTheUserIn (String user, String password){
        LoadLoginPresenter();
        presenter.receiveUserLogin(user, password);
    }
    // show spinBar
    private void spinProgressBar (){
        progressBar.setVisibility(View.VISIBLE);
    }

    // hide spinBar
    private void stopProgressBar(){
        progressBar.setVisibility(View.INVISIBLE);
    }

    //presenter view interface
    @Override
    public void onLoadSuccess() {
        stopProgressBar();
        Intent intent = new Intent(this,WeatherActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLoadError(String error) {
        stopProgressBar();
        Toast toast1 = Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT);
        toast1.show();
    }
}
