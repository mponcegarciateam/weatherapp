package com.example.mario.weatherapp.interactor;

/**
 * Created by Mario on 11/05/2016.
 */
public interface LoginInteractorInterface {
    public void attemptToLogin(String email,String password, Callback callback);

    interface Callback {
        void onSuccess();
        void onError(String error);
    }
}
