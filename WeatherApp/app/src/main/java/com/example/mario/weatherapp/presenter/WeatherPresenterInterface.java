package com.example.mario.weatherapp.presenter;

/**
 * Created by Mario on 11/05/2016.
 */
public interface WeatherPresenterInterface {
    public void receiveLocation(double longitude, double latitude);
}
