package com.example.mario.weatherapp.presenter;

/**
 * Created by Mario on 11/05/2016.
 */
public interface LoginPresenterInterface {
    public void receiveUserLogin (String email, String password);
}
