package com.example.mario.weatherapp.interactor.executor;

import android.os.Handler;
import android.os.Looper;

/**
 * Created by Mario on 17/05/2016.
 */
public class MainThreadImp implements MainThread{

    private Handler handler;

    public MainThreadImp() {
        this.handler = new Handler(Looper.getMainLooper());
    }

    public void post(Runnable runnable) {
        handler.post(runnable);
    }
}
