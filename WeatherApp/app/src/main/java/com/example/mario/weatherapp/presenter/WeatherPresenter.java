package com.example.mario.weatherapp.presenter;

import android.app.Activity;
import com.example.mario.weatherapp.interactor.WeatherInteractor;
import com.example.mario.weatherapp.interactor.WeatherInteractorInterface;
import com.example.mario.weatherapp.interactor.executor.MainThreadImp;
import com.example.mario.weatherapp.interactor.executor.MainThread;
import com.example.mario.weatherapp.model.WeatherData;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Mario on 10/05/2016.
 */
public class WeatherPresenter implements WeatherPresenterInterface {

    private View view;
    private WeatherInteractor interactor;

    public WeatherPresenter() {

    }

    public void setView(View view) {
        if (view == null) {
            throw new IllegalArgumentException("You can't set a null view");
        }
        this.view = view;
    }

    // Weather presenter interface
    // This method comunicate the ui with the interactor with a callback.
    @Override
    public void receiveLocation(double latitude, double longitude) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        MainThread mainThread = new MainThreadImp();
        interactor = new WeatherInteractor(latitude, longitude, executor, mainThread);
        interactor.execute(new WeatherInteractorInterface.Callback() {
            @Override
            public void onSuccess(WeatherData weather) {
                view.onLoadSuccess(weather);
            }

            @Override
            public void onError(String error) {
                view.onLoadError(error);
            }
        });
    }

    public interface View {
        void onLoadSuccess(WeatherData weather);
        void onLoadError(String error);
    }
}
