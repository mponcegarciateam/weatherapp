package com.example.mario.weatherapp;

import android.app.Application;
import android.content.Context;

import com.firebase.client.Firebase;

/**
 * Created by Mario on 11/05/2016.
 */
public class MyApp extends Application {

    static Context context;

    public MyApp() {
        super();
    }
    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        Firebase.setAndroidContext(getContext());

    }

    public static Context getContext() {
        return context;
    }

}
