package com.example.mario.weatherapp.model;

/**
 * Created by Mario on 25/05/2016.
 */
class Main {

    private final static String GRADES_CELCIUS ="ºC";
    private final static String FORMAT_NO_DECIMAL_VALUES="%.0f";
    private final static float GRADES_KELVIN = 273.15f;
    float temp;

    public String getTemp (){
        return String.format(FORMAT_NO_DECIMAL_VALUES, getTemperatureInCelsius()) + GRADES_CELCIUS;
    }

    public float getTemperatureInCelsius() {
        temp = temp - GRADES_KELVIN;
        return temp;
    }
}
