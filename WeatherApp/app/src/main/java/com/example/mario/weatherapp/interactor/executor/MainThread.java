package com.example.mario.weatherapp.interactor.executor;

/**
 * Created by Mario on 17/05/2016.
 */
public interface MainThread {
    void post(final Runnable runnable);
}
