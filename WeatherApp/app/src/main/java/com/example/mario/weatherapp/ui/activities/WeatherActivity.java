package com.example.mario.weatherapp.ui.activities;

import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.mario.weatherapp.MyApp;
import com.example.mario.weatherapp.R;
import com.example.mario.weatherapp.model.WeatherData;
import com.example.mario.weatherapp.presenter.WeatherPresenter;
import com.example.mario.weatherapp.ui.GeoUtils;
import com.squareup.picasso.Picasso;

public class WeatherActivity extends AppCompatActivity implements WeatherPresenter.View{

    public static double lat, lon;
    private static final int INITIAL_REQUEST = 1337;
    public static final String[] PERMISSION_LOCATION = {android.Manifest.permission.ACCESS_FINE_LOCATION};
    private WeatherPresenter presenter;
    private ImageView ivIcon;
    private TextView tvCity;
    private TextView tvTemperature;
    private TextView tvDescription;

    //AppCompatActivity methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        setItems();
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(PERMISSION_LOCATION, INITIAL_REQUEST);
            }
        } else {
            GeoUtils geo = new GeoUtils(getBaseContext());
            lat = geo.getLatitude();
            lon = geo.getLongitude();
        }
        presenter = new WeatherPresenter();
        presenter.setView(this);
        presenter.receiveLocation(lat, lon);
    }

    // WeatherActivity methods
    public void setItems() {
        ivIcon = (ImageView) findViewById(R.id.ivIcon);
        tvCity = (TextView) findViewById(R.id.tvCity);
        tvTemperature = (TextView) findViewById(R.id.tvTemperature);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
    }

    // Those methods return true if we can access to the location of the user
    private boolean canAccessLocation() {
        return(hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION));
    }

    @TargetApi (23)
    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED==checkSelfPermission(perm));
    }

    // presenter view interface
    @Override
    public void onLoadSuccess(WeatherData weather) {
        Picasso.with(MyApp.getContext()).load(weather.getIcon()).into(ivIcon);
        tvDescription.setText(weather.getDescription());
        tvCity.setText(weather.getName());
        tvTemperature.setText(weather.getTemp());
    }

    @Override
    public void onLoadError(String error) {
        Toast toast1 = Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT);
        toast1.show();
    }
}