package com.example.mario.weatherapp.model;

import com.google.gson.Gson;

import java.io.Reader;
import java.util.List;

/**
 * Created by Mario on 12/05/2016.
 */

public class WeatherData {

    private Main main;
    private String name;
    private List<Weather> weather;

    public String getName(){
        return name;
    }

    public String getTemp (){
        return main.getTemp();
    }

    public String getDescription(){
        return weather.get(0).getDescription();
    }

    public String getIcon() {
        return weather.get(0).getIcon();
    }

    public static WeatherData getWeatherInfo (Reader reader){
        return new Gson().fromJson(reader, WeatherData.class);
    }
}