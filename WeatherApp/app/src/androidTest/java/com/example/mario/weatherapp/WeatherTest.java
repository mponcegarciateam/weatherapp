package com.example.mario.weatherapp;

import android.test.AndroidTestCase;
import com.example.mario.weatherapp.model.WeatherData;
import com.google.gson.Gson;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class WeatherTest extends AndroidTestCase {
    public void testCanConvertStringToJson () {
        final String WeatherInfo = "{\"coord\":{\"lon\":-6.27,\"lat\":36.5},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"base\":\"cmc stations\",\"main\":{\"temp\":291.891,\"pressure\":1028.59,\"humidity\":91,\"temp_min\":291.891,\"temp_max\":291.891,\"sea_level\":1028.94,\"grnd_level\":1028.59},\"wind\":{\"speed\":1.66,\"deg\":9.5},\"clouds\":{\"all\":68},\"dt\":1463480609,\"sys\":{\"message\":0.0061,\"country\":\"ES\",\"sunrise\":1463462165,\"sunset\":1463513243},\"id\":6356927,\"name\":\"Cádiz\",\"cod\":200}";
        final Gson gson = new Gson();
        final WeatherData weather = gson.fromJson(WeatherInfo,WeatherData.class);
        assertEquals("broken clouds", weather.getDescription());
        assertEquals("Cádiz", weather.getName());
    }
}