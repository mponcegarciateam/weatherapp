# README #

This application use firebase to login on and shows you a little information about hows weather is in your location (latitude, longitude).

### WeatherApp activities ###

***1. MainActivity***
: This activity allow us to connect into firebase database. If it success, the application will show you a WeatherActivity.

>>![login.png](https://bitbucket.org/repo/oaonjo/images/933132704-login.png)

***2. WeatherActivity***
: This activity shows you information about your location weather.

>>![weather.png](https://bitbucket.org/repo/oaonjo/images/826452537-weather.png)

### Other Java Class and interfaces ###

We have 2 interactors and 2 presenter class that they will comunicate with the activities, giving them information to show you.

We also have some interfaces that we will use for those clases.

The class WeatherData will content all the information that we will use in the WeatherActivity.

### Tests ###

***1. WeatherTest ***
: This test check if we can convert a String into a WeatherData object using Json and Gson.

### Other information ###

*You can contact me on my twitter <https://twitter.com/Marioponcee>.*